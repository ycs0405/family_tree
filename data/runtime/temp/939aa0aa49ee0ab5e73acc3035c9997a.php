<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:50:"themes/admin_simpleboot3/admin\member\listing.html";i:1536333373;s:70:"E:\zupu\family_tree\public\themes\admin_simpleboot3\public\header.html";i:1536333373;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap">
    <h1>成员列表</h1>
    <form class="form-inline">
        <div class="form-group">
            <label for="name">成员名</label>
            <input class="form-control" id="name" name="name" value="<?php echo isset($params['name'])?$params['name']:''; ?>"/>
        </div>
        <div class="form-group">
            <label for="clan">房族</label>
            <select name="clan_id" id="clan" class="form-control">
                <option value="">请选择...</option>
                <option value="0" <?php if(isset($data) && $data['clan_id']==0): ?>selected="selected"<?php endif; ?>>无</option>
                <?php if(is_array($clan_tree) || $clan_tree instanceof \think\Collection || $clan_tree instanceof \think\Paginator): if( count($clan_tree)==0 ) : echo "" ;else: foreach($clan_tree as $key=>$vo): ?>
                    <option value="<?php echo $vo['id']; ?>" <?php if(isset($params['clan_id']) && $params['clan_id']==$vo['id']): ?>selected="selected"<?php endif; ?>><?php $__FOR_START_9113__=0;$__FOR_END_9113__=$vo['level'];for($i=$__FOR_START_9113__;$i < $__FOR_END_9113__;$i+=1){ ?>|-<?php } ?><?php echo $vo['name']; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="generation">世代</label>
            <input class="form-control" id="generation" type="number" min="1" value="<?php echo isset($params['generation'])?$params['generation']:''; ?>" name="generation"/>
        </div>
        <button type="submit" class="btn btn-primary">确定</button>
    </form>
    </div>
<div class="wrap">
    <table class="table">
        <thead>
            <tr>
                <th>名字</th>
                <th>所属房族</th>
                <th>性别</th>
                <th>家庭排行</th>
                <th>父亲</th>
                <th>丈夫</th>
                <th>世代</th>
                <th>创建时间</th>
                <th>更新时间</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
        <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
            <tr>
                <td><?php echo $vo['name']; ?></td>
                <td><?php echo $vo['clan_name']; ?></td>
                <td>
                    <?php switch($vo['gender']): case "0": ?>未知<?php break; case "1": ?>男<?php break; case "2": ?>女<?php break; endswitch; ?>
                </td>
                <td><?php echo $vo['ranking']==0?'未知':$vo['ranking']; ?></td>
                <td><?php echo $vo['father_name']; ?></td>
                <td><?php echo $vo['husband_name']; ?></td>
                <td><?php echo $vo['generation']; ?></td>
                <td><?php echo date('Y-m-d H:i:s',$vo['create_time']); ?></td>
                <td><?php echo date('Y-m-d H:i:s',$vo['update_time']); ?></td>
                <td>
                    <?php if($vo['generation']!=0): ?>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            添加成员
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo url('admin/member/memberAdd',['target_id'=>$vo['id'],'type'=>1]); ?>">添加子女</a></li>
                            <li><a href="<?php echo url('admin/member/memberAdd',['target_id'=>$vo['id'],'type'=>2]); ?>">添加配偶</a></li>
                        </ul>
                    </div>
                    <?php endif; if($vo['generation']!=1): ?>
                    <a href="<?php echo url('admin/member/memberEdit',['id'=>$vo['id']]); ?>" class="btn btn-primary">编辑成员</a>
                    <button class="btn btn-primary btn-delete" data-toggle="modal" data-target="#modalDelete" data-id="<?php echo $vo['id']; ?>">删除成员</button>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table>
    <div class="pagination"><?php echo (isset($page) && ($page !== '')?$page:''); ?></div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalDelete" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">是否确定删除</h4>
            </div>
            <div class="modal-body">
                您正在进行删除成员操作，如果您删除的是本族的成员，那么他的老婆以及子女都会被同时删除，确定吗？
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary" id="delSubmit" data-id="">确定</button>
            </div>
        </div>
    </div>

</div>
<script>
    $(function(){
       $('.btn-delete').click(function(){
           var id = $(this).attr('data-id');
           $('#delSubmit').attr('data-id',id);
       });
        $('#delSubmit').click(function(){
            var id = $(this).attr('data-id');
            location.href = '/admin/member/memberDelete/id/'+id;
        })
    });
</script>
</body>
</html>