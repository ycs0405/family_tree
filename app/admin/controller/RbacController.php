<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\ClanModel;
use app\admin\model\MemberModel;
use cmf\controller\AdminBaseController;
use think\Db;
use tree\Tree;
use app\admin\model\AdminMenuModel;

class RbacController extends AdminBaseController
{

    /**
     * 角色管理列表
     * @adminMenu(
     *     'name'   => '角色管理',
     *     'parent' => 'admin/User/default',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '角色管理',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        $data = Db::name('role')->order(["list_order" => "ASC", "id" => "DESC"])->select();
        $this->assign("roles", $data);
        return $this->fetch();
    }

    /**
     * 添加角色
     * @adminMenu(
     *     'name'   => '添加角色',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加角色',
     *     'param'  => ''
     * )
     */
    public function roleAdd()
    {
        return $this->fetch();
    }

    /**
     * 添加角色提交
     * @adminMenu(
     *     'name'   => '添加角色提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加角色提交',
     *     'param'  => ''
     * )
     */
    public function roleAddPost()
    {
        if ($this->request->isPost()) {
            $data   = $this->request->param();
            $result = $this->validate($data, 'role');
            if ($result !== true) {
                // 验证失败 输出错误信息
                $this->error($result);
            } else {
                $result = Db::name('role')->insert($data);
                if ($result) {
                    $this->success("添加角色成功", url("rbac/index"));
                } else {
                    $this->error("添加角色失败");
                }

            }
        }
    }

    /**
     * 编辑角色
     * @adminMenu(
     *     'name'   => '编辑角色',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '编辑角色',
     *     'param'  => ''
     * )
     */
    public function roleEdit()
    {
        $id = $this->request->param("id", 0, 'intval');
        if ($id == 1) {
            $this->error("超级管理员角色不能被修改！");
        }
        $data = Db::name('role')->where(["id" => $id])->find();
        if (!$data) {
            $this->error("该角色不存在！");
        }
        $this->assign("data", $data);
        return $this->fetch();
    }

    /**
     * 编辑角色提交
     * @adminMenu(
     *     'name'   => '编辑角色提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '编辑角色提交',
     *     'param'  => ''
     * )
     */
    public function roleEditPost()
    {
        $id = $this->request->param("id", 0, 'intval');
        if ($id == 1) {
            $this->error("超级管理员角色不能被修改！");
        }
        if ($this->request->isPost()) {
            $data   = $this->request->param();
            $result = $this->validate($data, 'role');
            if ($result !== true) {
                // 验证失败 输出错误信息
                $this->error($result);

            } else {
                if (Db::name('role')->update($data) !== false) {
                    $this->success("保存成功！", url('rbac/index'));
                } else {
                    $this->error("保存失败！");
                }
            }
        }
    }

    /**
     * 删除角色
     * @adminMenu(
     *     'name'   => '删除角色',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '删除角色',
     *     'param'  => ''
     * )
     */
    public function roleDelete()
    {
        $id = $this->request->param("id", 0, 'intval');
        if ($id == 1) {
            $this->error("超级管理员角色不能被删除！");
        }
        $count = Db::name('RoleUser')->where(['role_id' => $id])->count();
        if ($count > 0) {
            $this->error("该角色已经有用户！");
        } else {
            $status = Db::name('role')->delete($id);
            if (!empty($status)) {
                $this->success("删除成功！", url('rbac/index'));
            } else {
                $this->error("删除失败！");
            }
        }
    }

    /**
     * 设置角色权限
     * @adminMenu(
     *     'name'   => '设置角色权限',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '设置角色权限',
     *     'param'  => ''
     * )
     */
    public function authorize()
    {
        $AuthAccess     = Db::name("AuthAccess");
        $adminMenuModel = new AdminMenuModel();
        //角色ID
        $roleId = $this->request->param("id", 0, 'intval');
        if (empty($roleId)) {
            $this->error("参数错误！");
        }

        $tree       = new Tree();
        $tree->icon = ['│ ', '├─ ', '└─ '];
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';

        $result = $adminMenuModel->menuCache();
//        print_r(var_export($result));die;
//        $result = $this->testMenu();
//        print_r(implode(',',array_keys($result)));die;
        $newMenus      = [];
        $privilegeData = $AuthAccess->where(["role_id" => $roleId])->column("rule_name");//获取权限表数据
//        print_r($privilegeData);die;
        foreach ($result as $m) {
            $newMenus[$m['id']] = $m;
        }

        foreach ($result as $n => $t) {
            $result[$n]['checked']      = ($this->_isChecked($t, $privilegeData)) ? ' checked' : '';
            $result[$n]['level']        = $this->_getLevel($t['id'], $newMenus);
            $result[$n]['style']        = empty($t['parent_id']) ? '' : 'display:none;';
            $result[$n]['parentIdNode'] = ($t['parent_id']) ? ' class="child-of-node-' . $t['parent_id'] . '"' : '';
        }

        $str = "<tr id='node-\$id'\$parentIdNode  style='\$style'>
                   <td style='padding-left:30px;'>\$spacer<input type='checkbox' name='menuId[]' value='\$id' level='\$level' \$checked onclick='javascript:checknode(this);'> \$name</td>
    			</tr>";

        $tree->init($result);

        $category = $tree->getTree(0, $str);
        $this->assign("category", $category);
        $this->assign("roleId", $roleId);
        return $this->fetch();
    }

    /**
     * 角色授权提交
     * @adminMenu(
     *     'name'   => '角色授权提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '角色授权提交',
     *     'param'  => ''
     * )
     */
    public function authorizePost()
    {
        if ($this->request->isPost()) {
            $roleId = $this->request->param("roleId", 0, 'intval');
            if (!$roleId) {
                $this->error("需要授权的角色不存在！");
            }
            if (is_array($this->request->param('menuId/a')) && count($this->request->param('menuId/a')) > 0) {

                Db::name("authAccess")->where(["role_id" => $roleId, 'type' => 'admin_url'])->delete();
                foreach ($_POST['menuId'] as $menuId) {
                    $menu = Db::name("adminMenu")->where(["id" => $menuId])->field("app,controller,action")->find();
                    if ($menu) {
                        $app    = $menu['app'];
                        $model  = $menu['controller'];
                        $action = $menu['action'];
                        $name   = strtolower("$app/$model/$action");
                        Db::name("authAccess")->insert(["role_id" => $roleId, "rule_name" => $name, 'type' => 'admin_url']);
                    }
                }

                cache(null, 'admin_menus');// 删除后台菜单缓存

                $this->success("授权成功！");
            } else {
                //当没有数据时，清除当前角色授权
                Db::name("authAccess")->where(["role_id" => $roleId])->delete();
                $this->error("没有接收到数据，执行清除授权成功！");
            }
        }
    }

    /**
     * 检查指定菜单是否有权限
     * @param array $menu menu表中数组
     * @param $privData
     * @return bool
     */
    private function _isChecked($menu, $privData)
    {
        $app    = $menu['app'];
        $model  = $menu['controller'];
        $action = $menu['action'];
        $name   = strtolower("$app/$model/$action");
        if ($privData) {
            if (in_array($name, $privData)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /**
     * 获取菜单深度
     * @param $id
     * @param array $array
     * @param int $i
     * @return int
     */
    protected function _getLevel($id, $array = [], $i = 0)
    {
        if ($array[$id]['parent_id'] == 0 || empty($array[$array[$id]['parent_id']]) || $array[$id]['parent_id'] == $id) {
            return $i;
        } else {
            $i++;
            return $this->_getLevel($array[$id]['parent_id'], $array, $i);
        }
    }

    //角色成员管理
    public function member()
    {
        //TODO 添加角色成员管理

    }

    /*public function testMenu()
    {
        return    array (
            6 =>
                array (
                    'id' => 6,
                    'parent_id' => 0,
                    'type' => 0,
                    'status' => 1,
                    'list_order' => 0,
                    'app' => 'admin',
                    'controller' => 'Setting',
                    'action' => 'default',
                    'param' => '',
                    'name' => '设置',
                    'icon' => 'cogs',
                    'remark' => '系统设置入口',
                ),
            71 =>
                array (
                    'id' => 71,
                    'parent_id' => 6,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 0,
                    'app' => 'admin',
                    'controller' => 'Setting',
                    'action' => 'site',
                    'param' => '',
                    'name' => '网站信息',
                    'icon' => '',
                    'remark' => '网站信息',
                ),
            109 =>
                array (
                    'id' => 109,
                    'parent_id' => 0,
                    'type' => 0,
                    'status' => 1,
                    'list_order' => 10,
                    'app' => 'user',
                    'controller' => 'AdminIndex',
                    'action' => 'default',
                    'param' => '',
                    'name' => '用户管理',
                    'icon' => 'group',
                    'remark' => '用户管理',
                ),
            49 =>
                array (
                    'id' => 49,
                    'parent_id' => 109,
                    'type' => 0,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'User',
                    'action' => 'default',
                    'param' => '',
                    'name' => '管理组',
                    'icon' => '',
                    'remark' => '管理组',
                ),
            50 =>
                array (
                    'id' => 50,
                    'parent_id' => 49,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Rbac',
                    'action' => 'index',
                    'param' => '',
                    'name' => '角色管理',
                    'icon' => '',
                    'remark' => '角色管理',
                ),
            51 =>
                array (
                    'id' => 51,
                    'parent_id' => 50,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Rbac',
                    'action' => 'roleAdd',
                    'param' => '',
                    'name' => '添加角色',
                    'icon' => '',
                    'remark' => '添加角色',
                ),
            52 =>
                array (
                    'id' => 52,
                    'parent_id' => 50,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Rbac',
                    'action' => 'roleAddPost',
                    'param' => '',
                    'name' => '添加角色提交',
                    'icon' => '',
                    'remark' => '添加角色提交',
                ),
            53 =>
                array (
                    'id' => 53,
                    'parent_id' => 50,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Rbac',
                    'action' => 'roleEdit',
                    'param' => '',
                    'name' => '编辑角色',
                    'icon' => '',
                    'remark' => '编辑角色',
                ),
            54 =>
                array (
                    'id' => 54,
                    'parent_id' => 50,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Rbac',
                    'action' => 'roleEditPost',
                    'param' => '',
                    'name' => '编辑角色提交',
                    'icon' => '',
                    'remark' => '编辑角色提交',
                ),
            55 =>
                array (
                    'id' => 55,
                    'parent_id' => 50,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Rbac',
                    'action' => 'roleDelete',
                    'param' => '',
                    'name' => '删除角色',
                    'icon' => '',
                    'remark' => '删除角色',
                ),
            56 =>
                array (
                    'id' => 56,
                    'parent_id' => 50,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Rbac',
                    'action' => 'authorize',
                    'param' => '',
                    'name' => '设置角色权限',
                    'icon' => '',
                    'remark' => '设置角色权限',
                ),
            57 =>
                array (
                    'id' => 57,
                    'parent_id' => 50,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Rbac',
                    'action' => 'authorizePost',
                    'param' => '',
                    'name' => '角色授权提交',
                    'icon' => '',
                    'remark' => '角色授权提交',
                ),
            73 =>
                array (
                    'id' => 73,
                    'parent_id' => 6,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Setting',
                    'action' => 'password',
                    'param' => '',
                    'name' => '密码修改',
                    'icon' => '',
                    'remark' => '密码修改',
                ),
            74 =>
                array (
                    'id' => 74,
                    'parent_id' => 73,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Setting',
                    'action' => 'passwordPost',
                    'param' => '',
                    'name' => '密码修改提交',
                    'icon' => '',
                    'remark' => '密码修改提交',
                ),
            77 =>
                array (
                    'id' => 77,
                    'parent_id' => 6,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Setting',
                    'action' => 'clearCache',
                    'param' => '',
                    'name' => '清除缓存',
                    'icon' => '',
                    'remark' => '清除缓存',
                ),
            110 =>
                array (
                    'id' => 110,
                    'parent_id' => 49,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'User',
                    'action' => 'index',
                    'param' => '',
                    'name' => '管理员',
                    'icon' => '',
                    'remark' => '管理员管理',
                ),
            111 =>
                array (
                    'id' => 111,
                    'parent_id' => 110,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'User',
                    'action' => 'add',
                    'param' => '',
                    'name' => '管理员添加',
                    'icon' => '',
                    'remark' => '管理员添加',
                ),
            112 =>
                array (
                    'id' => 112,
                    'parent_id' => 110,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'User',
                    'action' => 'addPost',
                    'param' => '',
                    'name' => '管理员添加提交',
                    'icon' => '',
                    'remark' => '管理员添加提交',
                ),
            113 =>
                array (
                    'id' => 113,
                    'parent_id' => 110,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'User',
                    'action' => 'edit',
                    'param' => '',
                    'name' => '管理员编辑',
                    'icon' => '',
                    'remark' => '管理员编辑',
                ),
            114 =>
                array (
                    'id' => 114,
                    'parent_id' => 110,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'User',
                    'action' => 'editPost',
                    'param' => '',
                    'name' => '管理员编辑提交',
                    'icon' => '',
                    'remark' => '管理员编辑提交',
                ),
            115 =>
                array (
                    'id' => 115,
                    'parent_id' => 110,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'User',
                    'action' => 'userInfo',
                    'param' => '',
                    'name' => '个人信息',
                    'icon' => '',
                    'remark' => '管理员个人信息修改',
                ),
            116 =>
                array (
                    'id' => 116,
                    'parent_id' => 110,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'User',
                    'action' => 'userInfoPost',
                    'param' => '',
                    'name' => '管理员个人信息修改提交',
                    'icon' => '',
                    'remark' => '管理员个人信息修改提交',
                ),
            117 =>
                array (
                    'id' => 117,
                    'parent_id' => 110,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'User',
                    'action' => 'delete',
                    'param' => '',
                    'name' => '管理员删除',
                    'icon' => '',
                    'remark' => '管理员删除',
                ),
            118 =>
                array (
                    'id' => 118,
                    'parent_id' => 110,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'User',
                    'action' => 'ban',
                    'param' => '',
                    'name' => '停用管理员',
                    'icon' => '',
                    'remark' => '停用管理员',
                ),
            119 =>
                array (
                    'id' => 119,
                    'parent_id' => 110,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'User',
                    'action' => 'cancelBan',
                    'param' => '',
                    'name' => '启用管理员',
                    'icon' => '',
                    'remark' => '启用管理员',
                ),
            /*152 =>
                array (
                    'id' => 152,
                    'parent_id' => 109,
                    'type' => 0,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'user',
                    'controller' => 'AdminIndex',
                    'action' => 'default1',
                    'param' => '',
                    'name' => '用户组',
                    'icon' => '',
                    'remark' => '用户组',
                ),
            153 =>
                array (
                    'id' => 153,
                    'parent_id' => 152,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'user',
                    'controller' => 'AdminIndex',
                    'action' => 'index',
                    'param' => '',
                    'name' => '本站用户',
                    'icon' => '',
                    'remark' => '本站用户',
                ),
            154 =>
                array (
                    'id' => 154,
                    'parent_id' => 153,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'user',
                    'controller' => 'AdminIndex',
                    'action' => 'ban',
                    'param' => '',
                    'name' => '本站用户拉黑',
                    'icon' => '',
                    'remark' => '本站用户拉黑',
                ),
            155 =>
                array (
                    'id' => 155,
                    'parent_id' => 153,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'user',
                    'controller' => 'AdminIndex',
                    'action' => 'cancelBan',
                    'param' => '',
                    'name' => '本站用户启用',
                    'icon' => '',
                    'remark' => '本站用户启用',
                ),
            156 =>
                array (
                    'id' => 156,
                    'parent_id' => 152,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'user',
                    'controller' => 'AdminOauth',
                    'action' => 'index',
                    'param' => '',
                    'name' => '第三方用户',
                    'icon' => '',
                    'remark' => '第三方用户',
                ),
            157 =>
                array (
                    'id' => 157,
                    'parent_id' => 156,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'user',
                    'controller' => 'AdminOauth',
                    'action' => 'delete',
                    'param' => '',
                    'name' => '删除第三方用户绑定',
                    'icon' => '',
                    'remark' => '删除第三方用户绑定',
                ),
            158 =>
                array (
                    'id' => 158,
                    'parent_id' => 6,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'user',
                    'controller' => 'AdminUserAction',
                    'action' => 'index',
                    'param' => '',
                    'name' => '用户操作管理',
                    'icon' => '',
                    'remark' => '用户操作管理',
                ),
            159 =>
                array (
                    'id' => 159,
                    'parent_id' => 158,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'user',
                    'controller' => 'AdminUserAction',
                    'action' => 'edit',
                    'param' => '',
                    'name' => '编辑用户操作',
                    'icon' => '',
                    'remark' => '编辑用户操作',
                ),
            160 =>
                array (
                    'id' => 160,
                    'parent_id' => 158,
                    'type' => 2,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'user',
                    'controller' => 'AdminUserAction',
                    'action' => 'editPost',
                    'param' => '',
                    'name' => '编辑用户操作提交',
                    'icon' => '',
                    'remark' => '编辑用户操作提交',
                ),
            161 =>
                array (
                    'id' => 161,
                    'parent_id' => 158,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'user',
                    'controller' => 'AdminUserAction',
                    'action' => 'sync',
                    'param' => '',
                    'name' => '同步用户操作',
                    'icon' => '',
                    'remark' => '同步用户操作',
                ),
            162 =>
                array (
                    'id' => 162,
                    'parent_id' => 0,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Clan',
                    'action' => 'default',
                    'param' => '',
                    'name' => '房族管理',
                    'icon' => '',
                    'remark' => '房族管理',
                ),
            163 =>
                array (
                    'id' => 163,
                    'parent_id' => 162,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Clan',
                    'action' => 'listing',
                    'param' => '',
                    'name' => '房族列表',
                    'icon' => '',
                    'remark' => '房族列表',
                ),
            164 =>
                array (
                    'id' => 164,
                    'parent_id' => 162,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Clan',
                    'action' => 'add',
                    'param' => '',
                    'name' => '房族添加',
                    'icon' => '',
                    'remark' => '房族添加',
                ),
            165 =>
                array (
                    'id' => 165,
                    'parent_id' => 162,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Clan',
                    'action' => 'edit',
                    'param' => '',
                    'name' => '房族编辑',
                    'icon' => '',
                    'remark' => '房族编辑',
                ),
            166 =>
                array (
                    'id' => 166,
                    'parent_id' => 162,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Clan',
                    'action' => 'view',
                    'param' => '',
                    'name' => '房族查看',
                    'icon' => '',
                    'remark' => '房族查看',
                ),
            167 =>
                array (
                    'id' => 167,
                    'parent_id' => 162,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Clan',
                    'action' => 'delete',
                    'param' => '',
                    'name' => '房族删除',
                    'icon' => '',
                    'remark' => '房族删除',
                ),
            168 =>
                array (
                    'id' => 168,
                    'parent_id' => 0,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Prologue',
                    'action' => 'index',
                    'param' => '',
                    'name' => '班序编辑',
                    'icon' => '',
                    'remark' => '班序编辑',
                ),
            169 =>
                array (
                    'id' => 169,
                    'parent_id' => 0,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Epoch',
                    'action' => 'default',
                    'param' => '',
                    'name' => '纪元管理',
                    'icon' => '',
                    'remark' => '纪元管理',
                ),
            170 =>
                array (
                    'id' => 170,
                    'parent_id' => 169,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Epoch',
                    'action' => 'listing',
                    'param' => '',
                    'name' => '朝代列表',
                    'icon' => '',
                    'remark' => '朝代列表',
                ),
            171 =>
                array (
                    'id' => 171,
                    'parent_id' => 169,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Epoch',
                    'action' => 'add',
                    'param' => '',
                    'name' => '朝代添加',
                    'icon' => '',
                    'remark' => '朝代添加',
                ),
            172 =>
                array (
                    'id' => 172,
                    'parent_id' => 169,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Epoch',
                    'action' => 'edit',
                    'param' => '',
                    'name' => '朝代编辑',
                    'icon' => '',
                    'remark' => '朝代编辑',
                ),
            173 =>
                array (
                    'id' => 173,
                    'parent_id' => 169,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Epoch',
                    'action' => 'delete',
                    'param' => '',
                    'name' => '朝代删除',
                    'icon' => '',
                    'remark' => '朝代删除',
                ),
            174 =>
                array (
                    'id' => 174,
                    'parent_id' => 169,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Epoch',
                    'action' => 'view',
                    'param' => '',
                    'name' => '朝代查看',
                    'icon' => '',
                    'remark' => '朝代查看',
                ),
            175 =>
                array (
                    'id' => 175,
                    'parent_id' => 0,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Article',
                    'action' => 'default',
                    'param' => '',
                    'name' => '文章管理',
                    'icon' => '',
                    'remark' => '文章管理',
                ),
            176 =>
                array (
                    'id' => 176,
                    'parent_id' => 175,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Article',
                    'action' => 'listing',
                    'param' => '',
                    'name' => '文章列表',
                    'icon' => '',
                    'remark' => '文章列表',
                ),
            177 =>
                array (
                    'id' => 177,
                    'parent_id' => 175,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Article',
                    'action' => 'add',
                    'param' => '',
                    'name' => '文章添加',
                    'icon' => '',
                    'remark' => '文章添加',
                ),
            178 =>
                array (
                    'id' => 178,
                    'parent_id' => 175,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Article',
                    'action' => 'edit',
                    'param' => '',
                    'name' => '文章编辑',
                    'icon' => '',
                    'remark' => '文章编辑',
                ),
            179 =>
                array (
                    'id' => 179,
                    'parent_id' => 175,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Article',
                    'action' => 'delete',
                    'param' => '',
                    'name' => '文章删除',
                    'icon' => '',
                    'remark' => '文章删除',
                ),
            180 =>
                array (
                    'id' => 180,
                    'parent_id' => 175,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Article',
                    'action' => 'view',
                    'param' => '',
                    'name' => '文章查看',
                    'icon' => '',
                    'remark' => '文章查看',
                ),
            181 =>
                array (
                    'id' => 181,
                    'parent_id' => 0,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Member',
                    'action' => 'default',
                    'param' => '',
                    'name' => '成员管理',
                    'icon' => 'sitemap',
                    'remark' => '成员管理',
                ),
            182 =>
                array (
                    'id' => 182,
                    'parent_id' => 181,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Member',
                    'action' => 'masterAdd',
                    'param' => '',
                    'name' => '始祖添加',
                    'icon' => '',
                    'remark' => '始祖添加',
                ),
            183 =>
                array (
                    'id' => 183,
                    'parent_id' => 181,
                    'type' => 1,
                    'status' => 1,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Member',
                    'action' => 'masterEdit',
                    'param' => '',
                    'name' => '始祖编辑',
                    'icon' => '',
                    'remark' => '始祖编辑',
                ),
            184 =>
                array (
                    'id' => 184,
                    'parent_id' => 181,
                    'type' => 1,
                    'status' => 0,
                    'list_order' => 10000,
                    'app' => 'admin',
                    'controller' => 'Member',
                    'action' => 'listing',
                    'param' => '',
                    'name' => '成员列表',
                    'icon' => '',
                    'remark' => '成员列表',
                )
        );

    }*/

    public function familyMember()
    {

        $name = input('name/s');
        $clan_id = input('clan_id/s');
        $id = $this->request->param("id", 0, 'intval');
        $member_model = new MemberModel();
        $clan_model = new ClanModel();
        $role_member = Db::name('role_member');
        $role_list = (array)$role_member->where(['role_id'=>$id])->find();
        $list = $member_model->memberList($name,$clan_id);
        $clan_tree = $clan_model->clanTree(0);
        $this->assign('name',$name);
        $this->assign('list',$list);
        $this->assign('clan_tree',$clan_tree);
        $this->assign('clan_id',$clan_id);
        $this->assign('role_list',$role_list);
        $this->assign('id',$id);
        return $this->fetch();
    }

    public function familyMemberSubmit()
    {
        $roleId = input('id/d');
        if(!$roleId){
            $this->error('参数错误');
        }
        $member_id = input('member_id/d');
        $role_member = Db::name('role_member');

            if($role_member->where(['role_id'=>$roleId])->find()){
                if($member_id){
                    $rs = $role_member->where(['role_id'=>$roleId])->update(['role_id'=>$roleId,'member_id'=>$member_id]);
                }else{
                    $rs =true;
                }
            }else{
                if($member_id) {
                    $rs = $role_member->insertGetId(['role_id' => $roleId, 'member_id' => $member_id]);
                }else{
                    $this->error('请设置权限');
                }
            }

        if($rs){
            $this->success('设置成功',url('admin/rbac/index'));
        }else{
            $this->error('设置失败');
        }
    }

}

