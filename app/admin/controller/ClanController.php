<?php
/**
 * Created by PhpStorm.
 * User: 珊珊
 * Date: 2018/6/3
 * Time: 22:30
 */

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use app\admin\model\ClanModel;
use think\Request;

class ClanController extends AdminBaseController
{
    public function listing()
    {
        $name = input('name/s');
        $clan_model = new ClanModel();
        if($name){
            $this->assign('name',$name);
            $list = $clan_model->getListByName($name);
        }else{
            $list = $clan_model->clanTree(0);
        }
        $this->assign('list',$list);
        return $this->fetch();
    }

    public function add()
    {
        $clan_model = new ClanModel();
        if(Request::instance()->isPost()){
            //先校验
            $name = input('name/s');
            $parent_id = input('parent_id/s');
            if($parent_id === ''){
                $this->error('请选择上级房族');
            }
            if(!$name){
                $this->error('请输入房族名称');
            }
            $id = $clan_model->insertClan($name,input('detail/s'),(int)$parent_id);
            if(!$id){
                $this->error('插入失败');
            }else{
                $this->success('添加成功',url('admin/clan/listing'));
            }
        }else{
            $clanList = $clan_model->clanTreeTwo(0);
            $this->assign('clanList',$clanList);
            return $this->fetch();
        }

    }

    public function edit()
    {
        $id = input('id/d');
        if(!$id){
            $this->error('参数错误');
        }
        $clan_model = new ClanModel();
        //获取数据
        $data = $clan_model->where(['id'=>$id,'is_deleted'=>0])->find();
        if(!$data){
            $this->error('该记录不存在');
        }
        if(Request::instance()->isPost()){
            //验证
            $parent_id = input('parent_id/s');
            $name = input('name/s');
            if($parent_id === ''){
                $this->error('请选择上级房族');
            }
            if(!$name){
                $this->error('请输入房族名称');
            }
            //计算level
            $level = 0;
            if($parent_id) {
                $parent = $clan_model->where(['id' => $parent_id, 'is_deleted' => 0])->find();
                if($parent){
                    $level = $parent['level'] + 1;
                }
            }
            $update = [
                'name'=>$name,
                'parent_id'=>$parent_id,
                'detail'=>input('detail/s'),
                'update_time'=>time(),
                'level'=>$level
            ];
            $rs = $clan_model->updateById($id,$update);
            if($rs){
                $this->success('编辑成功',url('admin/clan/listing'));
            }
        }else{
            $clanList = $clan_model->clanTreeTwo(0);
            $this->assign('data',$data);
            $this->assign('clanList',$clanList);
            return $this->fetch('add');
        }

    }

    public function view()
    {
        $id = input('id/d');
        if(!$id){
            $this->error('参数错误');
        }
        $clan_model = new ClanModel();
        //获取数据
        $data = $clan_model->getById($id);
        if(!$data){
            $this->error('该记录不存在');
        }
        $this->assign('data',$data);
        return $this->fetch();
    }

    public function delete()
    {
        $id = input('id/d');
        if(!$id){
            $this->error('参数错误');
        }
        $clan_model = new ClanModel();

        $clan_model->cyclicDelete(3);
        $this->success('删除成功');
    }


}