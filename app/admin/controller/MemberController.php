<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/9
 * Time: 15:31
 */

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use app\admin\model\ClanModel;
use think\Request;
use app\admin\model\MemberModel;
use think\db;

class MemberController extends AdminBaseController
{
    public function listing()
    {
        //搜索条件
        $name = input('name/s');
        $clan_id = input('clan_id/d');
        $generation = input('generation/d');
        $params = [];
        if($name){
            $params['m.name'] = ['like','%'.$name.'%'];
        }
        if($clan_id){
            $params['m.clan_id'] = $clan_id;
        }
        if($generation){
            $params['m.generation'] = $generation;
        }
        $request = Request::instance()->param();

        //获取房族列表
        $clan_model = new ClanModel();
        $clan_list = $clan_model->clanTree(0);
        //获取成员列表
        $member_model = new MemberModel();
        $list = $member_model->listing($params);
        $list = $list->paginate(20,false,['query'=>$request]);
        $datas = $list->toArray();
        $this->assign('list',$datas['data']);
        //保持分页条件
        $this->assign('page',$list->render());
        $this->assign('params',$request);
        $this->assign('clan_tree',$clan_list);
        return $this->fetch();
    }

    public function masterAdd()
    {
        if (Request::instance()->isPost()) {
            //验证
            $name = input('name/s');
            $description = input('description/s');
            if (!$name) {
                $this->error('名字不能为空');
            }
            //构造数据数组
            $insert = [
                'name' => $name,
                'style_name'=>input('style_name/s'),
                'description' => $description,
                'gender'=>1,
                'ranking'=>0,
                'parent_id'=>0,
                'spouse_id'=>0,
                'generation'=>1,
                'create_time'=>time(),
                'update_time'=>time(),
                'is_deleted'=>0
            ];
            $member_model = new MemberModel();
            $res = $member_model->insert($insert);
            if($res){
                $this->success('添加成功',url('admin/member/listing'));
            }else{
                $this->error('添加失败');
            }

        } else {

            $clan_model = new ClanModel();
            $member_model = new MemberModel();
            if($member_model->where(['generation'=>1])->find()){
                $this->error('已存在始祖，请勿重复添加',url('admin/member/listing'));
            }
            $clan_tree = $clan_model->clanTree(0);
            $this->assign('clan_tree',$clan_tree);
            return $this->fetch();
        }
    }

    public function masterEdit()
    {
        $member_model = new MemberModel();
        if(Request::instance()->isPost()){
            //验证
            $name = input('name/s');
            $description = input('description/s');
            if (!$name) {
                $this->error('名字不能为空');
            }
            $update = [
                'name'=>$name,
                'description'=>$description,
                'style_name'=>input('style_name/s'),
                'update_time'=>time(),
                'clan_id'=>input('clan_id/d')
            ];
            $res = $member_model->where(['generation'=>1])->update($update);
            if($res){
                $this->success('修改成功');
            }else{
                $this->error('修改失败');
            }

        }else{
            $clan_model = new ClanModel();
            $data = $member_model->where(['generation'=>1])->find();
            $clan_tree = $clan_model->clanTree(0);
            $this->assign('clan_tree',$clan_tree);
            $this->assign('data',$data);
            return $this->fetch('master_add');
        }
    }

    public function memberAdd()
    {
        $target_id = input('target_id/d');
        $type = input('type/d');//1添加子女，2添加配偶
        if(!$target_id || !$type){
            $this->error('参数错误');
        }
        $auth = $this->checkAuth($target_id);
        if(!$auth){
            $this->error('您没有权限！',url('admin/member/listing'));
        }
        if(Request::instance()->isPost()){
            //检查权限

            //验证
            $name = input('name/s');
            if(!$name){
                $this->error('名不能为空');
            }
            $type = input('type/d');
            $member_model = new MemberModel();
            if($type == 1){
                //子女
                $ranking = input('ranking/d');
                if($ranking < 1){
                    $this->error('家庭排行不能小于1');
                }
                //计算世代
                $parent_id = input('target_id/d');
                $parent = $member_model->where(['id'=>$target_id])->find()->toArray();
                $insert = [
                    'name'=>$name,
                    'style_name'=>input('style_name/s'),
                    'clan_id'=>input('clan_id/d'),
                    'gender'=>input('gender/d'),
                    'ranking'=>$ranking,
                    'parent_id'=>$target_id,
                    'description'=>input('description/s'),
                    'generation'=>$parent['generation']+1,
                    'create_time'=>time(),
                    'update_time'=>time()
                ];
            }
            if($type == 2){
                //配偶
                $insert = [
                    'name'=>$name,
                    'style_name'=>'',
                    'clan_id'=>0,
                    'gender'=>2,
                    'ranking'=>0,
                    'parent_id'=>0,
                    'spouse_id'=>$target_id,
                    'description'=>input('description/s'),
                    'generation'=>0,
                    'create_time'=>time(),
                    'update_time'=>time()
                ];
            }
            $id = $member_model->insertGetId($insert);
            if($id){
                $this->success('添加成功',url('admin/member/listing'));
            }else{
                $this->error('添加失败');
            }

        }else{
            //读取房族列表
            $clan_model = new ClanModel();
            $clan_tree = $clan_model->clanTree(0);
            $this->assign('clan_tree',$clan_tree);
            $this->assign('type',$type);
            $this->assign('target_id',$target_id);
            return $this->fetch();
        }
    }

    public function memberDelete()
    {
        $id = input('id/d');
        if(!$id){
            $this->error('参数错误');
        }
        //验证
        $member_model = new MemberModel();
        $member = $member_model->where(['id'=>$id,'is_deleted'=>0])->find();
        if($member) {
            $member = $member->toArray();
            if($member['generation'] == 0){
                $auth = $this->checkAuth($member['spouse_id']);
            }else{
                $auth = $this->checkAuth($member['id']);
            }
            if(!$auth){
                $this->error('您没有权限！',url('admin/member/listing'));
            }
        }
        if(!$member){
            $this->error('成员不存在');
        }
        if($member['generation']==1){
            $this->error('始祖不能删除');
        }
        //删除
        $rs = $member_model->cyclicDelete($id);
        $this->success('删除成功');
    }

    public function memberEdit()
    {
        $member_model = new MemberModel();
        $id = input('id/d');
        if(!$id){
            $this->error('参数错误');
        }
        //检查权限

        $member = $member_model->where(['id'=>$id,'is_deleted'=>0])->find();
        if($member) {
            $member = $member->toArray();
            if($member['generation'] == 0){
                $auth = $this->checkAuth($member['spouse_id']);
            }else{
                $auth = $this->checkAuth($member['id']);
            }
            if(!$auth){
                $this->error('您没有权限！',url('admin/member/listing'));
            }
        }
        if(!$member){
            $this->error('成员不存在');
        }
        if(Request::instance()->isPost()){

            $type = input('type/d');//1添加子女，2添加配偶
            $name = input('name/s');
            $ranking = input('ranking/s');
            if(!$type){
                $this->error('参数错误');
            }
            if(!$name){
                $this->error('名不能为空');
            }
            if($type == 1){
                $update = [
                    'name'=>$name,
                    'style_name'=>input('style_name/s'),
                    'clan_id'=>input('clan_id/d'),
                    'gender'=>input('gender/d'),
                    'ranking'=>$ranking,
                    'update_time'=>time(),
                    'description'=>input('description/s')
                ];
            }
            if($type == 2){
                //配偶
                $update = [
                    'name'=>$name,
                    'description'=>input('description/s'),
                    'update_time'=>time()
                ];
            }

            $rs = $member_model->where(['id'=>$id,'is_deleted'=>0])->update($update);
            if($rs){
                $this->success('编辑成功');
            }else{
                $this->error('编辑失败');
            }
        }else{
            if($member['generation']){
                //本氏族的成员
                $type = 1;
                $parent_id = input('target_id/d');
            }else{
                //氏族成员的老铺
                $type = 2;
            }
            //读取房族列表
            $clan_model = new ClanModel();
            $clan_tree = $clan_model->clanTree(0);
            $this->assign('clan_tree',$clan_tree);
            $this->assign('data',$member);
            $this->assign('type',$type);
            $this->assign('id',$id);
            return $this->fetch('member_add');
        }
    }

    private function checkAuth($id)
    {
        $admin_id = cmf_get_current_admin_id();
        if($admin_id == 1){
            return true;
        }
        $role_member = Db::name('role_user')
            ->alias('ru')
            ->join('role_member rm','rm.role_id=ru.role_id')
            ->where(['ru.role_id'=>$admin_id])
            ->column('rm.member_id');
        $member_model = new MemberModel();
        $rs = $member_model->relationChan($id);
        foreach ($role_member as $item) {
            if(in_array($item,$rs)){
                return true;
            }
        }
        return false;

    }


}
