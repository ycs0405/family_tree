<?php

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Request;
use app\admin\model\ArticleModel;

class ArticleController extends AdminBaseController
{
    public function listing()
    {
        $article_model = new ArticleModel();
        $where = ['is_deleted'=>0];
        $title = input('title/s');
        $param = [];
        if($title){
            $param['title'] = $title;
        }
        if($title){
            $where['title'] = ['like','%'.$title.'%'];
        }
        $list = $article_model->where($where)->field('id,title,create_time,update_time')->paginate(20,false,['query'=>$param]);
        $this->assign('page',$list->render());
        if($list){
            $list = $list->toArray();
            $this->assign('list',$list['data']);
        }else{
            $this->assign('list',[]);
        }
        $this->assign('title',$title);
        return $this->fetch();

    }

    public function add()
    {
        if(Request::instance()->isPost()){
            $title = input('title/s');
            if(!$title){
                $this->error('标题不能为空');
            }
            $content = input('content/s');
            if(!$content){
                $this->error('内容不能为空');
            }

            $insert = [
                'title'=>$title,
                'content'=>$content,
                'create_time'=>time(),
                'update_time'=>time(),
                'is_deleted'=>0
            ];

            $article_model = new ArticleModel();
            $id = $article_model->insertGetId($insert);
            if($id){
                $this->success('添加成功');
            }else{
                $this->error('添加失败');
            }
        }else{
            return $this->fetch();
        }
    }

    public function edit()
    {
        $article_model = new ArticleModel();
        if(Request::instance()->isPost()){
            $id = input('id/d');
            if(!$id){
                $this->error('参数错误');
            }
            $title = input('title/s');
            if(!$title){
                $this->error('标题不能为空');
            }
            $content = input('content/s');
            if(!$content){
                $this->error('内容不能为空');
            }
            $update = [
                'title'=>$title,
                'content'=>$content,
                'update_time'=>time(),
                'is_deleted'=>0
            ];
            $res = $article_model->where(['id'=>$id,'is_deleted'=>0])->update($update);
            if($res){
                $this->success('修改成功',url('admin/article/listing'));
            }else{
                $this->error('修改失败',url('admin/article/listing'));
            }
        }else{
            $id = input('id/d');
            if(!$id){
                $this->error('参数错误');
            }
            $data = $article_model->where(['id'=>$id,'is_deleted'=>0])->find()->toArray();
            if(!$data){
                $this->error('文章不存在',url('admin/article/listing'));
            }
            $this->assign('data',$data);
            return $this->fetch('add');
        }
    }

    public function delete()
    {
        $id = input('id/d');
        if(!$id){
            $this->error('参数错误');
        }
        $article_model = new ArticleModel();
        $article_model->where(['id'=>$id])->update(['is_deleted'=>1]);
        $this->success('删除成功');
    }

    public function view()
    {
        $id = input('id/d');
        if(!$id){
            $this->error('参数错误');
        }
        $article_model = new ArticleModel();
        $data = $article_model->where(['id'=>$id,'is_deleted'=>0])->find()->toArray();
        if(!$data){
            $this->error('文章不存在',url('admin/article/listing'));
        }
        $this->assign('data',$data);
        return $this->fetch();
    }
}