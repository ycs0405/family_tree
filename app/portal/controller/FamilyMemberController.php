<?php
/**
 * Created by PhpStorm.
 * User: 珊珊
 * Date: 2018/7/12
 * Time: 21:29
 */

namespace app\portal\controller;
use think\controller\Rest;
use think\Db;
use app\portal\model\MemberModel;

class FamilyMemberController extends Rest
{
    public function getTree()
    {
        $trunkId = input('trunk_id/d');//初始成员的id
        $deep = input('deep/d');//深度
        //先得到初始成员的世代
        $member_model = new MemberModel();
        $trunk = $member_model->where(['id'=>$trunkId,'is_deleted'=>0,'generation'=>['gt',0]])->field('id,name,gender,ranking,parent_id,generation')->find();
        if($trunk){
            $trunk = $trunk->toArray();
            (array)$list = $member_model->memberTree($trunkId,$trunk['generation'],$deep);
            if($list){
                $trunk['has_child'] = true;
            }
            $list = array_merge([$trunk],$list);
            return cmf_api_json(true,$list);
        }else{
            return cmf_api_json(false,[],'没有数据');
        }

    }
}