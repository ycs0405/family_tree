<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/28
 * Time: 21:53
 */

namespace app\portal\controller;
use app\portal\model\ArticleModel;
use think\controller\Rest;

class FamilyArticleController extends Rest
{
    /**
     * 获取文章列表（分页）
     */
    public function getList(){
        $article_model = new ArticleModel();
        $page = input('page/d');
		 if(!$page){
            $page = 1;
        }
        $data = $article_model->getList($page);
        if($data){
            // $data = $data->toArray();
            return cmf_api_json(true,$data,'成功');
        }else{
            $data = [];
            return cmf_api_json(false,$data,'没有数据');
        }
    }

    /**
     * @return \think\response\Json
     * @throws \think\Exception
     */
    public function getData()
    {
        $id = input('id/d');
        if(!$id){
            return cmf_api_json(false,[],'参数错误');
        }
        $article_model = new ArticleModel();
        $data = $article_model->getById($id);
        if($data){
            $data = $data->toArray();
            $data['content'] = html_entity_decode($data['content']);
            return cmf_api_json(true,$data,'成功');
        }else{
            $data = [];
            return cmf_api_json(false,$data,'没有数据');
        }

    }
}