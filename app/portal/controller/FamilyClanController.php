<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/28
 * Time: 22:13
 */

namespace app\portal\controller;

use think\Request;
use app\portal\model\ClanModel;
use think\controller\Rest;

class FamilyClanController extends Rest
{
    /**
     * 获取顶级族群列表
     * @return \think\response\Json
     */
    public function getParentList()
    {
        $page = input('page/d');
		 if(!$page){
            $page = 1;
        }
		$model_clan = new ClanModel();
        $data = $model_clan->getParentList($page);
        if($data){
            return cmf_api_json(true,$data);
        }else{
            return cmf_api_json(false,[],'没有数据');
        }
    }
	
	/**
     * 获取二级族房列表
     * @return \think\response\Json
     */
    public function getFangList()
    {
        $page = input('page/d');
		$pId = input('pid/d');
        $keyword = input('keyword/s');
		 if(!$page){
            $page = 1;
        }
		$model_clan = new ClanModel();
        $data = $model_clan->getFangList($pId, $page, $keyword);
        if($data){
            return cmf_api_json(true,$data);
        }else{
            return cmf_api_json(false,[],'没有数据');
        }
    }
	/**
     * 获取三级级族房列表
     * @return \think\response\Json
     */
    public function getLeverList()
    {
		$pId = input('pid/d');
		$model_clan = new ClanModel();
        $data = $model_clan->getLeverList($pId);
        if($data){
            return cmf_api_json(true,$data);
        }else{
            return cmf_api_json(true,[],'没有数据');
        }
    }
}