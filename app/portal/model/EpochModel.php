<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/28
 * Time: 22:16
 */

namespace app\portal\model;
use think\Model;


class EpochModel extends Model
{
    /**
     * 获取列表（不分页）
     * @param $field
     * @return mixed
     *
     */
    public function getList($field)
    {
        return $this->where(['is_deleted'=>0])->field($field)->order('id', 'asc')->select();

    }

    /**
     * 获取朝代列表（不含详情）
     * @return mixed
     */
    public function getListSimple()
    {
        return $this->getList('id,name');
    }

    /**
     * 获取朝代列表（含详情）
     * @return mixed
     */
    public function getListComplex()
    {
        return $this->getList('id,name,detail');
    }

    /**
     * 获取朝代详情
     * @return array|false|\PDOStatement|string|Model
     */
    public function getById($id)
    {
        return $this->where(['id'=>$id,'is_deleted'=>0])->field('id,name,detail')->find();
    }


}