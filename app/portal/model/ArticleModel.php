<?php
/**
 * Created by PhpStorm.
 * User: 珊珊
 * Date: 2018/6/17
 * Time: 21:47
 */

namespace app\portal\model;
use think\Model;
use think\db;

class ArticleModel extends Model
{

    public function baseSelect($where,$offset=0,$limit=1000,$field='*',$order = 'id desc')
    {
        return $this
            ->where($where)
            ->limit($offset,$limit)
            ->order($order)
            ->field($field)
            ->select();
    }

    public function baseGet($where = [],$field = '*')
    {
        return $this->where($where)->field($field)->find();
    }

    public function getList($page = 1, $pageSize = 15)
    {
        $offset = 0;
		$limit = $pageSize;
		if($page > 1){
			$offset = ($page - 1) * $pageSize;
			$limit = $page * $pageSize;
		}
		
		$count = $this->where(['is_deleted'=>0])->count('id');
		$list = $this->baseSelect(['is_deleted'=>0],$offset,$limit,'id,title');
		$data['page'] = $page;
		$data['last'] = $count <= $limit;
		$data['list'] = $list->toArray();
		return $data;
    }

    public function getById($id)
    {
        return $this->baseGet(['id'=>$id,'is_deleted'=>0],'id,title,content');
    }

}